import React from 'react';
import { Root } from "native-base";
import { createAppContainer, createStackNavigator } from "react-navigation";
import ActionSheetExample from './ActionSheetExample';


const AppNavigator = createAppContainer(createStackNavigator(
  {
    ActionSheetExample: { screen: ActionSheetExample},
    
  },{
    headerMode: 'none'
  }
))

export default () =>
  <Root>
    <AppNavigator />
  </Root>;